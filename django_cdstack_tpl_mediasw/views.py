from django.template import engines

import copy

from django_cdstack_deploy.django_cdstack_deploy.func import (
    generate_config_static,
    get_host_vars,
    zip_add_file,
)
from django_cdstack_models.django_cdstack_models.models import *
from django_cdstack_tpl_deb_buster.django_cdstack_tpl_deb_buster.views import (
    handle as handle_deb_buster,
)
from django_cdstack_tpl_deb.django_cdstack_tpl_deb.views import handle as handle_deb
from django_cdstack_tpl_deb.django_cdstack_tpl_deb.views import get_os_release
from django_cdstack_tpl_deb_stretch.django_cdstack_tpl_deb_stretch.views import (
    get_apt_packages,
    get_apt_sources,
    get_apt_prefs,
)
from django_cdstack_tpl_crowdsec_main.django_cdstack_tpl_crowdsec_main.views import (
    handle as handle_crowdsec_main,
)
from django_cdstack_tpl_fail2ban.django_cdstack_tpl_fail2ban.views import (
    handle as handle_fail2ban,
)
from django_cdstack_tpl_ipcop.django_cdstack_tpl_ipcop.views import (
    handle as handle_ipcop,
)
from django_cdstack_tpl_wazuh_agent.django_cdstack_tpl_wazuh_agent.views import (
    handle as handle_wazuh_agent,
)
from django_cdstack_tpl_whclient.django_cdstack_tpl_whclient.views import (
    handle as handle_whclient,
)


def handle(zipfile_handler, template_opts, cmdb_host: CmdbHost):
    django_engine = engines["django"]

    module_prefix = "django_cdstack_tpl_mediasw/django_cdstack_tpl_mediasw"

    if "apt_packages" not in template_opts:
        template_opts["apt_packages"] = get_apt_packages(
            module_prefix + "/templates/image/imageconf/vmtemplate.yml"
        )

    if "fail2ban_apps" not in template_opts:
        template_opts["fail2ban_apps"] = copy.deepcopy(template_opts["apt_packages"])

    if "apt_sources" not in template_opts:
        template_opts["apt_sources"] = get_apt_sources(
            module_prefix + "/templates/image/imageconf/vmtemplate.yml"
        )

    if "apt_preferences" not in template_opts:
        template_opts["apt_preferences"] = get_apt_prefs(
            module_prefix + "/templates/image/imageconf/vmtemplate.yml"
        )

    if "os_release" not in template_opts:
        template_opts["os_release"] = get_os_release(
            module_prefix + "/templates/image/imageconf/vmtemplate.yml"
        )

    if "fail2ban_apps" not in template_opts:
        template_opts["fail2ban_apps"] = list()

    template_opts["fail2ban_apps"].append("asterisk")

    primary_group = cmdb_host.cmdbhostgroups_set.first().group_rel
    all_proxy_nodes = CmdbHost.objects.filter(cmdbhostgroups__group_rel=7)
    all_callswitch_nodes = CmdbHost.objects.filter(cmdbhostgroups__group_rel=1)
    all_other_mediasw_nodes = CmdbHost.objects.filter(
        cmdbhostgroups__group_rel=primary_group
    ).exclude(id=cmdb_host.id)

    template_opts["peer_list"] = list()
    template_opts["proxy_list"] = list()
    template_opts["callswitch_list"] = list()

    for node in all_other_mediasw_nodes:
        template_opts["peer_list"].append(get_host_vars(node))

    for node in all_callswitch_nodes:
        template_opts["callswitch_list"].append(get_host_vars(node))

    for proxy in all_proxy_nodes:
        template_opts["proxy_list"].append(
            {
                "hostname": proxy.hostname,
                "ipv4": proxy.cmdbvarshost_set.get(name="network_iface_eth0_ip").value,
            }
        )

    for node in all_other_mediasw_nodes:
        special_opts = get_host_vars(node)

        config_template_file = open(
            module_prefix + "/templates/config-fs/dynamic/etc/tinc/voipnet/hosts/peer",
            "r",
        ).read()
        config_template = django_engine.from_string(config_template_file)

        zip_add_file(
            zipfile_handler,
            "etc/tinc/voipnet/hosts/" + special_opts["node_hostname"],
            config_template.render(special_opts),
        )

        config_template_file = open(
            module_prefix
            + "/templates/config-fs/dynamic/var/lib/asterisk/keys/peer.pub",
            "r",
        ).read()
        config_template = django_engine.from_string(config_template_file)

        zip_add_file(
            zipfile_handler,
            "var/lib/asterisk/keys/" + special_opts["node_hostname"] + ".pub",
            config_template.render(special_opts),
        )

    config_template_file = open(
        module_prefix + "/templates/config-fs/dynamic/var/lib/asterisk/keys/peer.pub",
        "r",
    ).read()
    config_template = django_engine.from_string(config_template_file)

    zip_add_file(
        zipfile_handler,
        "var/lib/asterisk/keys/" + template_opts["node_hostname"] + ".pub",
        config_template.render(template_opts),
    )

    config_template_file = open(
        module_prefix
        + "/templates/config-fs/dynamic/etc/tinc/voipnet/hosts/peer-local",
        "r",
    ).read()
    config_template = django_engine.from_string(config_template_file)

    zip_add_file(
        zipfile_handler,
        "etc/tinc/voipnet/hosts/" + template_opts["node_hostname"],
        config_template.render(template_opts),
    )

    config_template_file = open(
        module_prefix + "/templates/config-fs/dynamic/var/lib/asterisk/keys/peer.key",
        "r",
    ).read()
    config_template = django_engine.from_string(config_template_file)

    zip_add_file(
        zipfile_handler,
        "var/lib/asterisk/keys/" + template_opts["node_hostname"] + ".key",
        config_template.render(template_opts),
    )

    generate_config_static(zipfile_handler, template_opts, module_prefix)
    handle_whclient(zipfile_handler, template_opts, cmdb_host, skip_handle_os)
    handle_ipcop(zipfile_handler, template_opts, cmdb_host, skip_handle_os)
    handle_fail2ban(zipfile_handler, template_opts, cmdb_host, skip_handle_os)
    handle_crowdsec_main(zipfile_handler, template_opts, cmdb_host, skip_handle_os)
    handle_wazuh_agent(zipfile_handler, template_opts, cmdb_host, skip_handle_os)

    if not skip_handle_os:
        handle_deb(zipfile_handler, template_opts, cmdb_host, skip_handle_os)

    return True
